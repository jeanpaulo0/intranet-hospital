$(document).ready(function(){

  function hideAllMenus(){
      $(".menu-main-item > ul").each(function(){
        $(this).css("display", "none");

        if ( $(this).hasClass('show-subitem') ) {
            $(this).removeClass('show-subitem');
        } 
      });
      $(".menu-setores-item > ul").each(function(){
        $(this).css("display", "none");

        if ($(this).hasClass('show-subitem')) {
            $(this).removeClass('show-subitem');
        } 
      });     
  }

  function toggleSubMenu(obj){
      hideAllMenus();
      
      var hide_menu = $(obj).next();

      $(this).on('click', function(){
          $(hide_menu).toggleClass("show-subitem");  
      });
  };

  $("._has-sub-item > a").on('click', function(){
      toggleSubMenu(this);
  });

  function removeLinks(object, debug) {
      var obj = $(object);

      $(obj).each(function(){

        $(this).attr("href", "javascript:void(0)");
        if (debug) {
          console.log(this.text + " - Link Removido - " + this.href);  
        }        
      });

  }  


  function setJumboImgs(debug) {

      var jumbo_images = [];
      var jumbo_array = [];
      var jumbotron = $(".jumbotron-features");

      var tamanho = $(jumbotron).find(".servicos-item").size();
      var jObj = $(jumbotron).find(".servicos-item");
      var temp_link = [];
      var temp_img = [];
      var temp_alt = [];

      for (var i = 0 ; i < tamanho; i++) {
        var obj_jumbo = {jLink:null,jImg:null,jAlt:null};

        debug ? console.log(i) : null;
        debug ? console.log($(jObj)[i]) : null;

        var iterate = $(jObj)[i];

        temp_link[i] = $(iterate).find("a").attr("href");
        temp_img[i] = $(iterate).find("img").attr("src");
        temp_alt[i] = $(iterate).find("img").attr("alt");

        debug ? console.log(temp_link[i]) : null;
        debug ? console.log(temp_img[i]) : null;
        debug ? console.log(temp_alt[i]) : null;

        obj_jumbo.jLink = temp_link[i];
        obj_jumbo.jImg  = temp_img[i];
        obj_jumbo.jAlt  = temp_alt[i];

        debug ? console.log(obj_jumbo) : null;

        jumbo_array.push(obj_jumbo);
      };
      debug ? console.log(jumbo_array) : null;

      $(".jumbotron-features").find("img").each(function(){
          var image = $(this).attr("src");
          var link  = $(this).parent().attr("href");
          jumbo_images.push([image,link]);
      });

      // $(".jumbotron-features").addClass("_total-hide");
      $(jumbotron).addClass("_total-hide");

      $("<div class='new-jumbo' id='new-jumbo'></div>").insertAfter(jumbotron);


      for (var i = 0 ; i < tamanho; i++) {
        // console.log(jumbo_array[i]);
        
        var jLink = jumbo_array[i].jLink;
        var jImg  = jumbo_array[i].jImg;
        var jAlt  = jumbo_array[i].jAlt;

        $("#new-jumbo").append("<div><a href='"+jLink+"'><img src='"+jImg+"' alt='"+jAlt+"'></a></div>");
      }

      $(".new-jumbo").slick({
          lazyLoad: 'ondemand',
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 600,
          infinite: true,
          autoplay: true,
          autoplaySpeed: 4000,
          dots:true
      });

      $(".new-jumbo").find("button").each(function(){
        $(this).css("position", "static");
        $(this).css("display", "inline-block");

        if (this.innerText == "Previous") {
            $(this).css("margin-top", "20px");
        }
        
        if (this.innerText == "Next") {        
            $(this).css("margin-top", "20px");
        }
        
      });

      $(".jumbo-container img").each(function(){
          $(this).css("display","inline-block");
      })

  };

  hideAllMenus();
  removeLinks("._has-sub-item > a", false);  
  setJumboImgs(false);


  // Clicar numi tem proximo a um .has-sub-item - fecha os menus, menos o proprio has-sub-item
  $(document).on('click', function(event) {
      if (!$(event.target).closest('._has-sub-item').length) {
          hideAllMenus();
      };
  });
  

});










