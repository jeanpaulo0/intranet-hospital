## Next changes ##
- Analyze why the main menu get behind the footer when it's opened
- Organize CSS and JS.


## Implementing ##
**[28/05/16]** - Maybe a click event on body element to make all menus close. **It was working, but toggleMenu fix broken it**


## Done ##
**[28/05/16]** - JS: Fix "setJumboImgs" function to make it get images, links and alt (attribute) properly from Jumbo container;

**[28/05/16]** - Clicking a button when it is opened, it has to close. It's not happening. Probably something about "toggleMenu" function -> siblings;

**[25/05/16]**- Adjust click events on menu - When the button is clicked, other buttons "close". If the same button is clicked (while opened), it needs to close. Today this is not happening.

(teste)
teste JEAN